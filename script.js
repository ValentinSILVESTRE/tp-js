// ! ===========================================================================================================================!
// !                                                                                                                            !
// !                                                          Variables                                                         !
// !                                                                                                                            !
// ! ===========================================================================================================================!

/** @type {HTMLInputElement} - L'input de recherche de film */
const MOVIE_SEARCH = document.querySelector('#movieSearch');

/** @type {HTMLHeadingElement} - Le titre de la section films */
const MOVIE_HEADING = document.querySelector('#movies__heading');

/** @type {HTMLUListElement} - La liste des films trouvés */
const MOVIE_LIST = document.querySelector('#movies__list');

// ! ===========================================================================================================================!
// !                                                                                                                            !
// !                                                         Fonctions                                                          !
// !                                                                                                                            !
// ! ===========================================================================================================================!

/**
 * Fonction éxécuté quand on soumet le formulaire de recherche de film
 */
const onSubmit = () => {
	/** - Nom du film recherché */
	const MOVIE_NAME = MOVIE_SEARCH.value;

	// Si la recherche est vide on ne fait rien
	if (MOVIE_NAME.length === 0) return;

	fetch(`https://www.omdbapi.com/?s=${MOVIE_NAME}&apikey=f6e256e1`)
		.then((response) => response.json())
		.then((data) => {
			// * - On supprime tous les films précédement trouvés
			MOVIE_LIST.innerHTML = '';

			/** - Vrai si on a trouvé au moins un film */
			const FOUND = data.Response === 'True';

			if (FOUND) {
				MOVIE_HEADING.classList.add('text-success');
				MOVIE_HEADING.classList.remove('text-danger');
				MOVIE_LIST.classList.remove('d-none');
				const MOVIE_COUNT = +data.Search.length;
				MOVIE_HEADING.textContent = `${MOVIE_COUNT} film${
					MOVIE_COUNT > 1 ? 's' : ''
				} correspondant${
					MOVIE_COUNT > 1 ? 's' : ''
				} à ${MOVIE_NAME} trouvé${MOVIE_COUNT > 1 ? 's' : ''} :`;
				const MOVIES = data.Search;

				for (const MOVIE of MOVIES) {
					const li = document.createElement('li');
					li.classList.add(
						'col-6',
						'col-sm-4',
						'col-md-3',
						'm-1',
						'mb-4',
						'p-3',
						'border',
						'rounded',
						'bg-secondary',
						'text-light',
						'd-flex',
						'flex-column',
						'justify-content-between'
					);

					const preview = document.createElement('img');
					preview.setAttribute('src', MOVIE.Poster);
					preview.classList.add('w-100');
					const title = document.createElement('h4');
					title.textContent = MOVIE.Title;
					title.classList.add('m-0', 'mt-3', 'mb-auto');
					const details = document.createElement('p');
					let type = 'film';
					let abbrTitle = 'Movie';
					if (MOVIE.Type === 'series') {
						type = 'tv';
						abbrTitle = 'Series';
					}
					if (MOVIE.Type === 'episode') {
						type = 'fast';
						abbrTitle = 'Episode';
					}
					details.innerHTML = `<span>Année : ${MOVIE.Year}</span><abbr title="${abbrTitle}"><i class="bi bi-${type}"></i></abbr>`;
					details.classList.add(
						'd-flex',
						'justify-content-between',
						'm-0'
					);

					// * On ajoute tous les éléments
					MOVIE_LIST.appendChild(li);
					li.appendChild(preview);
					li.appendChild(title);
					li.appendChild(details);
				}
			} else {
				MOVIE_HEADING.textContent = `Aucun film correspondant à ${MOVIE_NAME} n'a été trouvé.`;
				MOVIE_HEADING.classList.add('text-danger');
				MOVIE_HEADING.classList.remove('text-success');
				MOVIE_LIST.classList.add('d-none');
			}
		});
};
